set "srcDir=C:\Apps\projects\eai\eai-services\working-page-service"
set /p srcDir="source directory (default 'C:\Apps\projects\eai\eai-services\working-page-service'): "

cd %srcDir%

call mvn clean
call mvn package

docker build -t eartymiuk444/working-page-service-image .
docker push eartymiuk444/working-page-service-image
pause