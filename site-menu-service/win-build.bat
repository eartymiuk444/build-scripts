set "srcDir=C:\Apps\projects\eai\eai-services\site-menu-service"
set /p srcDir="source directory (default 'C:\Apps\projects\eai\eai-services\site-menu-service'): "

cd %srcDir%

call mvn clean
call mvn package

docker build -t eartymiuk444/site-menu-service-image .
docker push eartymiuk444/site-menu-service-image
pause