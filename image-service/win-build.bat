set "srcDir=C:\Apps\projects\eai\eai-services\image-service"
set /p srcDir="source directory (default 'C:\Apps\projects\eai\eai-services\image-service'): "

cd %srcDir%

call mvn clean
call mvn package

docker build -t eartymiuk444/image-service-image .
docker push eartymiuk444/image-service-image
pause