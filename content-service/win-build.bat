set "srcDir=C:\Apps\projects\eai\eai-services\content-service"
set /p srcDir="source directory (default 'C:\Apps\projects\eai\eai-services\content-service'): "

cd %srcDir%

call mvn clean
call mvn package

docker build -t eartymiuk444/content-service-image .
docker push eartymiuk444/content-service-image
pause