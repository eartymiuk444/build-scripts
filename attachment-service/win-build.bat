set "srcDir=C:\Apps\projects\eai\eai-services\attachment-service"
set /p srcDir="source directory (default 'C:\Apps\projects\eai\eai-services\attachment-service'): "

cd %srcDir%

call mvn clean
call mvn package

docker build -t eartymiuk444/attachment-service-image .
docker push eartymiuk444/attachment-service-image
pause