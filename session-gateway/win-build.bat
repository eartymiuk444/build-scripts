set "srcDir=C:\apps\projects\eai\eai-services\session-gateway"
set /p srcDir="source directory (default 'C:\apps\projects\eai\eai-services\session-gateway'): "

cd %srcDir%

call mvn clean
call mvn package

docker build -t eartymiuk444/session-gateway-image .
docker push eartymiuk444/session-gateway-image
pause