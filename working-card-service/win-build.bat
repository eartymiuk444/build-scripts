set "srcDir=C:\Apps\projects\eai\eai-services\working-card-service"
set /p srcDir="source directory (default 'C:\Apps\projects\eai\eai-services\working-card-service'): "

cd %srcDir%

call mvn clean
call mvn package

docker build -t eartymiuk444/working-card-service-image .
docker push eartymiuk444/working-card-service-image
pause