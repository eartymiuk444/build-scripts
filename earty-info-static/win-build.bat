set "srcDir=C:\Apps\projects\eai\eai-frontend\earty-info-static"
set /p srcDir="source directory (default 'C:\Apps\projects\eai\eai-frontend\earty-info-static'): "

cd %srcDir%
call npm install
call npm run generate

docker build -t eartymiuk444/earty-info-static-image .
docker push eartymiuk444/earty-info-static-image
pause